package vista;

import controlador.Controlador;

public class VentanaFiguras extends javax.swing.JFrame {

    public VentanaFiguras() {
        initComponents();
    }

    public void addEventos() {
        Controlador controlador = new Controlador(this);
        menuAbrir.addActionListener(controlador);
        menuNuevo.addActionListener(controlador);
        menuGuardar.addActionListener(controlador);
        bgFiguras.add(rbLinea);
        bgFiguras.add(rbTriangulo);
        bgFiguras.add(rbRectangulo);
        bgFiguras.add(rbPentagono);
        bgFiguras.add(rbPoligono);
        panelPrincipal.addMouseListener(controlador);
        spLado.addChangeListener(controlador);
        spG.addChangeListener(controlador);
        spB.addChangeListener(controlador);
        btMasRotar.addActionListener(controlador);
        btMenosRotar.addActionListener(controlador);
        btMasTamanio.addActionListener(controlador);
        btMenosTamanio.addActionListener(controlador);
    }

    @SuppressWarnings("unchecked")
  // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
  private void initComponents() {

    bgFiguras = new javax.swing.ButtonGroup();
    rbLinea = new javax.swing.JRadioButton();
    rbTriangulo = new javax.swing.JRadioButton();
    rbRectangulo = new javax.swing.JRadioButton();
    rbPentagono = new javax.swing.JRadioButton();
    etiquetaContador = new javax.swing.JLabel();
    jLabel4 = new javax.swing.JLabel();
    jLabel6 = new javax.swing.JLabel();
    jLabel7 = new javax.swing.JLabel();
    etiquetaLinea = new javax.swing.JLabel();
    etiquetaRectangulo = new javax.swing.JLabel();
    etiquetaPoligono = new javax.swing.JLabel();
    etiquetaColor = new javax.swing.JLabel();
    etiquetaAzul = new javax.swing.JLabel();
    etiquetaRojo = new javax.swing.JLabel();
    etiquetaVerde = new javax.swing.JLabel();
    etiquetaFiguras = new javax.swing.JLabel();
    pnColor = new javax.swing.JPanel();
    rbPoligono = new javax.swing.JRadioButton();
    etiquetaEstrella = new javax.swing.JLabel();
    etiquetaLados = new javax.swing.JLabel();
    spB = new javax.swing.JSpinner();
    spLado = new javax.swing.JSpinner();
    spG = new javax.swing.JSpinner();
    spR = new javax.swing.JSpinner();
    etiquetaTriangulo = new javax.swing.JLabel();
    etiquetaTransformaciones = new javax.swing.JLabel();
    cbFiguras = new javax.swing.JComboBox<>();
    panelPrincipal = new vista.Panel();
    btMasTamanio = new javax.swing.JButton();
    btMasRotar = new javax.swing.JButton();
    btMenosTamanio = new javax.swing.JButton();
    btMenosRotar = new javax.swing.JButton();
    menuArchivo = new javax.swing.JMenuBar();
    jMenu1 = new javax.swing.JMenu();
    menuNuevo = new javax.swing.JMenuItem();
    jSeparator1 = new javax.swing.JPopupMenu.Separator();
    menuAbrir = new javax.swing.JMenuItem();
    menuGuardar = new javax.swing.JMenuItem();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("Figuras Primitivas");
    setResizable(false);
    getContentPane().setLayout(null);

    rbLinea.setSelected(true);
    getContentPane().add(rbLinea);
    rbLinea.setBounds(10, 30, 20, 30);
    getContentPane().add(rbTriangulo);
    rbTriangulo.setBounds(10, 60, 20, 30);
    getContentPane().add(rbRectangulo);
    rbRectangulo.setBounds(10, 90, 20, 30);
    getContentPane().add(rbPentagono);
    rbPentagono.setBounds(10, 120, 21, 30);

    etiquetaContador.setText("Puntos: 0");
    getContentPane().add(etiquetaContador);
    etiquetaContador.setBounds(20, 490, 60, 20);
    getContentPane().add(jLabel4);
    jLabel4.setBounds(10, 480, 0, 0);

    jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/tamaño.png"))); // NOI18N
    getContentPane().add(jLabel6);
    jLabel6.setBounds(10, 450, 20, 30);

    jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/rotar.png"))); // NOI18N
    getContentPane().add(jLabel7);
    jLabel7.setBounds(10, 410, 20, 40);

    etiquetaLinea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/linea.png"))); // NOI18N
    getContentPane().add(etiquetaLinea);
    etiquetaLinea.setBounds(40, 30, 50, 30);

    etiquetaRectangulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/rectangulo.png"))); // NOI18N
    getContentPane().add(etiquetaRectangulo);
    etiquetaRectangulo.setBounds(40, 90, 30, 30);

    etiquetaPoligono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pentagono.png"))); // NOI18N
    getContentPane().add(etiquetaPoligono);
    etiquetaPoligono.setBounds(40, 120, 30, 30);

    etiquetaColor.setText("Color:");
    getContentPane().add(etiquetaColor);
    etiquetaColor.setBounds(10, 220, 40, 14);

    etiquetaAzul.setText("B:");
    getContentPane().add(etiquetaAzul);
    etiquetaAzul.setBounds(10, 310, 20, 20);

    etiquetaRojo.setText("R:");
    getContentPane().add(etiquetaRojo);
    etiquetaRojo.setBounds(10, 250, 20, 20);

    etiquetaVerde.setText("G:");
    getContentPane().add(etiquetaVerde);
    etiquetaVerde.setBounds(10, 280, 20, 20);

    etiquetaFiguras.setText("Figuras:");
    getContentPane().add(etiquetaFiguras);
    etiquetaFiguras.setBounds(10, 10, 60, 14);

    pnColor.setBackground(new java.awt.Color(102, 51, 255));

    javax.swing.GroupLayout pnColorLayout = new javax.swing.GroupLayout(pnColor);
    pnColor.setLayout(pnColorLayout);
    pnColorLayout.setHorizontalGroup(
      pnColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 20, Short.MAX_VALUE)
    );
    pnColorLayout.setVerticalGroup(
      pnColorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 20, Short.MAX_VALUE)
    );

    getContentPane().add(pnColor);
    pnColor.setBounds(50, 220, 20, 20);
    getContentPane().add(rbPoligono);
    rbPoligono.setBounds(10, 150, 20, 30);

    etiquetaEstrella.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/poligono.png"))); // NOI18N
    getContentPane().add(etiquetaEstrella);
    etiquetaEstrella.setBounds(40, 150, 30, 30);

    etiquetaLados.setText("L:");
    getContentPane().add(etiquetaLados);
    etiquetaLados.setBounds(10, 180, 20, 20);

    spB.setModel(new javax.swing.SpinnerNumberModel(255, 0, 255, 1));
    getContentPane().add(spB);
    spB.setBounds(30, 310, 40, 20);

    spLado.setModel(new javax.swing.SpinnerNumberModel(2, 2, 9, 1));
    getContentPane().add(spLado);
    spLado.setBounds(30, 180, 40, 20);

    spG.setModel(new javax.swing.SpinnerNumberModel(51, 0, 255, 1));
    getContentPane().add(spG);
    spG.setBounds(30, 280, 40, 20);

    spR.setModel(new javax.swing.SpinnerNumberModel(102, 0, 255, 1));
    getContentPane().add(spR);
    spR.setBounds(30, 250, 40, 20);

    etiquetaTriangulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/triangulo.png"))); // NOI18N
    getContentPane().add(etiquetaTriangulo);
    etiquetaTriangulo.setBounds(40, 60, 30, 30);

    etiquetaTransformaciones.setText("Transformaciones:");
    getContentPane().add(etiquetaTransformaciones);
    etiquetaTransformaciones.setBounds(10, 350, 110, 14);

    cbFiguras.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Figura..." }));
    cbFiguras.setToolTipText("");
    getContentPane().add(cbFiguras);
    cbFiguras.setBounds(10, 380, 90, 20);

    panelPrincipal.setBackground(new java.awt.Color(255, 255, 255));

    javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
    panelPrincipal.setLayout(panelPrincipalLayout);
    panelPrincipalLayout.setHorizontalGroup(
      panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 650, Short.MAX_VALUE)
    );
    panelPrincipalLayout.setVerticalGroup(
      panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
      .addGap(0, 500, Short.MAX_VALUE)
    );

    getContentPane().add(panelPrincipal);
    panelPrincipal.setBounds(120, 10, 650, 500);

    btMasTamanio.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
    btMasTamanio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
    btMasTamanio.setText("+t");
    btMasTamanio.setBorderPainted(false);
    btMasTamanio.setContentAreaFilled(false);
    getContentPane().add(btMasTamanio);
    btMasTamanio.setBounds(40, 450, 30, 20);

    btMasRotar.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
    btMasRotar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
    btMasRotar.setText("+r");
    btMasRotar.setBorderPainted(false);
    btMasRotar.setContentAreaFilled(false);
    getContentPane().add(btMasRotar);
    btMasRotar.setBounds(40, 420, 30, 20);

    btMenosTamanio.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
    btMenosTamanio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menos.png"))); // NOI18N
    btMenosTamanio.setText("-t");
    btMenosTamanio.setBorderPainted(false);
    btMenosTamanio.setContentAreaFilled(false);
    getContentPane().add(btMenosTamanio);
    btMenosTamanio.setBounds(70, 450, 30, 20);

    btMenosRotar.setFont(new java.awt.Font("Tahoma", 0, 3)); // NOI18N
    btMenosRotar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/menos.png"))); // NOI18N
    btMenosRotar.setText("-r");
    btMenosRotar.setBorderPainted(false);
    btMenosRotar.setContentAreaFilled(false);
    getContentPane().add(btMenosRotar);
    btMenosRotar.setBounds(70, 420, 30, 20);

    jMenu1.setText("Archivo");

    menuNuevo.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_N, java.awt.event.InputEvent.CTRL_DOWN_MASK));
    menuNuevo.setText("Nuevo");
    jMenu1.add(menuNuevo);
    jMenu1.add(jSeparator1);

    menuAbrir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_DOWN_MASK));
    menuAbrir.setText("Abrir");
    jMenu1.add(menuAbrir);

    menuGuardar.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_DOWN_MASK));
    menuGuardar.setText("Guardar");
    jMenu1.add(menuGuardar);

    menuArchivo.add(jMenu1);

    setJMenuBar(menuArchivo);

    pack();
  }// </editor-fold>//GEN-END:initComponents

  // Variables declaration - do not modify//GEN-BEGIN:variables
  public javax.swing.ButtonGroup bgFiguras;
  public javax.swing.JButton btMasRotar;
  public javax.swing.JButton btMasTamanio;
  public javax.swing.JButton btMenosRotar;
  public javax.swing.JButton btMenosTamanio;
  public javax.swing.JComboBox<String> cbFiguras;
  private javax.swing.JLabel etiquetaAzul;
  private javax.swing.JLabel etiquetaColor;
  public javax.swing.JLabel etiquetaContador;
  private javax.swing.JLabel etiquetaEstrella;
  private javax.swing.JLabel etiquetaFiguras;
  private javax.swing.JLabel etiquetaLados;
  private javax.swing.JLabel etiquetaLinea;
  private javax.swing.JLabel etiquetaPoligono;
  private javax.swing.JLabel etiquetaRectangulo;
  private javax.swing.JLabel etiquetaRojo;
  private javax.swing.JLabel etiquetaTransformaciones;
  private javax.swing.JLabel etiquetaTriangulo;
  private javax.swing.JLabel etiquetaVerde;
  private javax.swing.JLabel jLabel4;
  private javax.swing.JLabel jLabel6;
  private javax.swing.JLabel jLabel7;
  private javax.swing.JMenu jMenu1;
  private javax.swing.JPopupMenu.Separator jSeparator1;
  public javax.swing.JMenuItem menuAbrir;
  private javax.swing.JMenuBar menuArchivo;
  public javax.swing.JMenuItem menuGuardar;
  public javax.swing.JMenuItem menuNuevo;
  public vista.Panel panelPrincipal;
  public javax.swing.JPanel pnColor;
  public javax.swing.JRadioButton rbLinea;
  public javax.swing.JRadioButton rbPentagono;
  public javax.swing.JRadioButton rbPoligono;
  public javax.swing.JRadioButton rbRectangulo;
  public javax.swing.JRadioButton rbTriangulo;
  public javax.swing.JSpinner spB;
  public javax.swing.JSpinner spG;
  public javax.swing.JSpinner spLado;
  public javax.swing.JSpinner spR;
  // End of variables declaration//GEN-END:variables
}
