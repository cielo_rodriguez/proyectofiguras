package figurasprimitivas;

import vista.VentanaFiguras;

/**
 *
 * @author Cielo Rodriguez
 */
public class FigurasPrimitivas {

  public static void main(String[] args) {
    VentanaFiguras ventana = new VentanaFiguras();
    ventana.setLocation(100, 100);
    ventana.setSize(800, 600);
    ventana.addEventos();
    ventana.setVisible(true);
  }
  
}
